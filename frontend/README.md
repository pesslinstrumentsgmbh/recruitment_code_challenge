## Front-end developer test assignment

Dear developer, here are the details for the test provided by Pessl Instruments. The goal of this test is to create a JavaScript application (or TypeScript if you wish to use it) that displays a chart, a table of data for a temperature (avg, min, max) and rain sensor from a Pessl Instruments weather station.

The user should be able to choose the period (24 hours, 2 days, 7 days, 30 days) and the resolution (raw, hourly, daily) and both chart and table should be updated accordingly.

You may use any tools, libraries you may wish to use and you feel comfortable with. Don't overdo this challenge. The most important thing is clarity/readability of your code.  

Bonus points:

+ try to minimize the amount of data to fetch 
+ apply reactive programming principles
+ draw the chart directly on the canvas element

You find sample code for authorization & API access in the file [hmac_js_sample.html](hmac_js_sample.html).

The only part of the API call that the developer must alter (programatically) is the request URI: /data/{stationId}/{resolution}/last/{period}
Use 00000264 as stationId. The second parameter accepts the resolution values [ raw, hourly, daily, monthly ] and the last parameter is a period string (eg. 24h - 24 hours, 2d - 2 days, 7d - 7 days etc).

Find an example of returned JSON from the API (stripped for length) in [response_sample.json](response_sample.json). A sample mockup is also provided in [mockup.png](mockup.png).

If there are any unclarities please don't hesitate contacting us.


Best regards,