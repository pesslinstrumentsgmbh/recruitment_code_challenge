## Mobile developer test assignment

Dear developer, here are the details for the test provided by Pessl Instruments. The goal of this test is to create a mobile application that displays a chart, a table of data for a temperature (avg, min, max) and rain sensor from a Pessl Instruments weather station.

The user should be able to choose the period (24 hours, 2 days, 7 days, 30 days) and the resolution (raw, hourly, daily) and both chart and table should be updated accordingly.

You may use any tools, libraries you may wish to use and you feel comfortable with. Don't overdo this challenge. The most important thing is clarity/readability of your code. We would like you to use an architecture of your own choice. Try to separate Presentation Logic from the Business Logic. The project should be very easy to unit test. The project should have at least one unit test.

For authenticating on Pessl Instruments API you have two options. You can either calculate the HMAC hash from the provided public and private keys or use the OAuth 2.0 protocol for authenticting

### HMAC

You find sample code (written in JavaScript) for authorization & API access in the file [hmac_js_sample.html](hmac_js_sample.html).

### OAuth 2.0

For retrieving the access code using OAuths use the following parameters 

Auth URL : https://oauth.fieldclimate.com/authorize  
Access Token URL : https://oauth.fieldclimate.com/token

```
{
    "grant_type" : "password"
    "grant_type" : "refresh_token"
    "client_id" : "FieldclimateNG"
    "client_secret" :  "618a5baf48287eecbdfc754e9c933a"
    "username" :  "metosLocal"
    "password" :  "metos"
}
```

### Parsing data from the API

The only part of the API call that the developer must alter (programatically) is the request URI: /data/{stationId}/{resolution}/last/{period}
Use 00000264 as stationId. The second parameter accepts the resolution values [ raw, hourly, daily, monthly ] and the last parameter is a period string (eg. 24h - 24 hours, 2d - 2 days, 7d - 7 days etc).

Find an example of returned JSON from the API (stripped for length) in [response_sample.json](response_sample.json). A sample mockup is also provided in [mockup.png](mockup.png).

The developer should provide a working APK bundle and the source code.

If there are any unclarities please don't hesitate contacting us.

Best regards,